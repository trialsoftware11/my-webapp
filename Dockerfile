FROM php:7.2-apache
COPY . /var/www/html/ 
COPY ports.conf /etc/apache2/
RUN chmod -R 777 /var/run/apache2/
RUN apt-get update && apt-get -y install vim


